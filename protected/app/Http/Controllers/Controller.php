<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /* RETURN RESULT */
    public function returnResult($data)
    {
    	if ($data && !empty($data)) return ['status' => 'success', 'result' => $data];
    	elseif (empty($data)) return ['status' => 'fail', 'messages' => ['empty']];
    	else return ['status' => 'fail', 'messages' => ['failed to retrieve data']];
    }

    /* RETURN EXECUTION */
    public function returnExe($data)
    {
    	if ($data && !empty($data)) return ['status' => 'success'];
    	else return ['status' => 'fail'];
    }
}
