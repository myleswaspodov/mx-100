<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'jobs';

    protected $primaryKey = 'id_jobs';

    protected $fillable = ['jobs', 'created_at', 'updated_at'];

}
