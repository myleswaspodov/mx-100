<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:api'] ], function(){
	/* APPLY */
	Route::group(['prefix' => 'apply', 'middleware' => ['feature_control:worker']], function() {
	    Route::post('/', 'ApplyController@apply');
	});

	/* PROPOSAL */
	Route::group(['prefix' => 'proposal', 'middleware' => ['feature_control:employer']], function() {
	    Route::any('/', 'ProposalController@listProposal');
	});
});
